<p align="center"><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/c6/Moodle-logo.svg/1200px-Moodle-logo.svg.png"></p>

## Integrantes

 - Andres Sanchez Castrillon asanchezc@eafit.edu.co
   
  - Jose Alejandro Roman Patiño jaromanp@eafit.edu.co
   
   - Manuela Valencia Toro mvalanciat@eafit.edu.co

## Roles
- Andres Sanchez Castrillon:
	- QA: Seguridad
	- Despliegue con docker en DCA
	- Integración a base de datos RDS
	 - Gestión del dominio
 - Jose Alejandro Roman Patiño 
	  - QA: Rendimiento
	 - Documentación
	 - Asignación de tareas  
	  - Despliegue docker Amazon 
	  - Moodle Admin
  - Manuela Valencia Toro 
	  - QA: Disponibilidad
	  - Proovedor de servicios AWS
	  - Gestión de balanceadores y Autoscalling
	  - Documentación
## Acerca de moodle
[Moodle](https://docs.moodle.org/all/es/Acerca_de_Moodle) es una plataforma de aprendizaje diseñada para proporcionar a los educadores, administradores y alumnos un único sistema robusto, seguro e integrado para crear entornos de aprendizaje personalizados. Moodle está construido por el proyecto Moodle que está dirigido y coordinado por Moodle HQ, una compañía australiana de 30 desarrolladores que cuenta con el apoyo financiero de una red de más de 60 empresas de servicios Moodle Partner en todo el mundo.

## Arquitectura
<p align="center"><img src="http://imgfz.com/i/zCnrORa.png"></p>

## Requisitos no funcionales
### Disponibilidad
 - El sistema debde estar siempre disponible
 - El sistma debe contar con un almacenamiento total de 500 GB.
 - En caso de que sea necesario, el almacenamiento debe poder escalarse sin modificar la arquitectura.
 - El sistema debe almacenar toda la informacion de manera que persista ante posible fallas


### Rendimiento
- El sistema debe tener un tiempo de respuesta menor a 1 segundo.
- La concurrencia simultanea de usuarios sera de 2000.
- La base de datos debera soportar la cantidad de peticiones que puedan generar estos usuarios concurrentes.
- Se dispondra de un sistema CDN (content delivery network) que permita la distribución a nivel mundial de los contenidos de la plataforma, de forma que sean accesibles de una manera más eficiente desde diferente lugares del mundo. 

### Seguridad

 - El sistema debe permitir autenticación de usuarios por medio de usuario y contraseña y utilizando servicios de terceros que garanticen su identidad.
 - El aplicativo del sistema debe permitir conexión segura a través de un navegador de internet.
 - El sistema debe contar con un protocolo para restablecimiento de contraseña.

## Diseño para la escalabilidad
### Patrones de arquitectura
#### Disponibilidad
*Principio:* Redundancia de servicios.
*Impacto:* 
 - Si una de las instancias falla o requiere mayor capacidad, otras pueden soportarlo, de manera que el servicio siempre este disponible.

*Principio:* Redundancia en la persistencia
*Impacto:* 
 - Se tienen 2 bases de datos de manera que si la conexion a una de ellas falla, el sistema se conecte a la otra y no halla perdida de servicio o almacenamiento.

*Principio:* Monitoreo constante de cada componente del sistema
*Impacto:* 
 - Permite obtener reportes en tiempo del funcionamiento tanto interno como externo de cada componente.

*Principio:* Escalamiento horizontal
*Impacto:* 
 - Con el auto Scaling es posible que el numero de instancias incremente o disminuya a medida que sea necesario automaticamente.

*Principio:* Tolerancia a fallas
*Impacto:* 
 - El balanceador de cargas detecta las posibles fallas y cambia inmediatamente de instancia, permitiendo que el servicio este siempre disponible para el usuario.

*Principio:* Escalamiento en la persistencia
*Impacto:* 
 - En la base de datos, a medida que sea requerido, se producira un escalamiento en la capacidad de almacenamiento.

*Principio:* Backups
*Impacto:* 
 - Se posee un servicio de backups de manera que no se pierdan datos en ningun instante.

#### Rendimiento
*Principio:* Peso ligero, permitiendo funcionalidades básicas desde el home.
*Impacto:* 
 - Compresión de assets estaticos.
 - Minimizar peticiones de recursos .
 - Mejor experiencia de usuario en la pagina inicial.
 
*Principio:* Carga bajo demanda y [Lazy loading](https://dzone.com/articles/practical-php-patterns/practical-php-patterns-lazy) para archivos grandes.
*Impacto:* 
- Reducción del tamaño de la página .
- Percepción de carga más rápida.

*Principio:*  Estrategia de almacenamiento en caché precargado para 
datos usados frecuentemente.
*Impacto:* 
 - Reducción de los request de ida y vuelta en la red y optimización de entrega.

*Principio:* Combinación y minimización de archivos JS y CSS.
*Impacto:* 
 - Reduce el tamaño de archivos JS y CSS.
 - Reduce peticiones de recursos.

*Principio:* Aprovechamiento del almacenamiento cache en el navegador, Servidor web de cache y el cache en el CDN.
*Impacto:* 
 - Mejoramiento de la renderización de assets.
 - Mejoramiento de la carga de la pagina a través de distintas geografías.

#### Seguridad
*Problema:* Ataque a contraseñas
*Política:* 
- Longitud de al menos 8 caracteres, inclusión de caracteres mayúsculos, minúsculos, alfabéticos, numéricos, y no alfanuméricos.

*Problema:* Acceso cuentas y usuarios
*Política:* 
- Manejo de privilegios por tipo de usuario, y jerarquía de estos
- Eliminación de login como invitado

*Problema:* Administración de la sesión.
*Política:* 
- Se evitan varias sesiones por usuario.
- Se estableción tiempo límite de inactividad dentro del sistema.
- Todas las transacciones de información se realizan con protocolo https.

*Problema:* Autenticación y autorización.
*Política:* 
- Se definen criterios mínimos para establecimiento de contraseñas.
- Se define tiempo de expiración de contraseñas.
- Se utiliza autenticación con terceros.

## Herramientas y Tecnologías 
### Docker
[Docker](https://www.docker.com/) es una plataforma de software que le permite crear, probar e implementar aplicaciones rápidamente. Docker empaqueta software en unidades estandarizadas llamadas [contenedores](https://aws.amazon.com/es/containers/) que incluyen todo lo necesario para que el software se ejecute, incluidas bibliotecas, herramientas de sistema, código y tiempo de ejecución. Con Docker, puede implementar y ajustar la escala de aplicaciones rápidamente en cualquier entorno con la certeza de saber que su código se ejecutará.

### Cloud Computing
La computación en la nube ([cloud computing](aws.amazon.com/Free/CloudComputing%20%E2%80%8E%20%E2%80%8E)) es una tecnología que permite acceso remoto a softwares, almacenamiento de archivos y procesamiento de datos por medio de Internet, siendo así, una alternativa a la ejecución en una computadora personal o servidor local. En el modelo de nube, no hay necesidad de instalar aplicaciones localmente en computadoras.

La computación en la nube ofrece a los individuos y a las empresas la capacidad de un pool de recursos de computación con buen mantenimiento, seguro, de fácil acceso y bajo demanda.

### CDN (CloudFlare)
[CDN](https://www.cloudflare.com/es-la/cdn/) se corresponde con las siglas  _content delivery network_  y, como su propio nombre indica en inglés, se trata de una red de entrega de contenidos compuesta por diferentes ordenadores. Es decir, CDN hace referencia a un conjunto de  servidores ubicados en diferentes puntos geográficos que contienen copias de contenidos alojados en otros servidores.
CloudFlare es un sistema gratuito que actúa como un proxy (intermediario) entre los visitantes del sitio y el servidor. Al actuar como un proxy, CloudFlare guarda temporalmente contenido estático del sitio, el cual disminuye el número de peticiones al servidor pero sigue permitiendo a los visitantes el acceso al sitio.

### SSL
SSL es una tecnología estandarizada que permite cifrar el tráfico de datos entre un navegador web y un sitio web (o entre dos servidores web), protegiendo así la conexión. Esto impide que un hacker pueda ver o interceptar la información que se transmite de un punto a otro, y que puede incluir datos personales o financieros.

### Balanceador de carga
[Elastic Load Balancing](https://aws.amazon.com/es/elasticloadbalancing/) distribuye automáticamente el tráfico de aplicaciones entrantes a través de varios destinos, tales como instancias de Amazon EC2, contenedores, direcciones IP y funciones Lambda. Permite controlar la carga variable del tráfico de una aplicación en una única zona o en varias zonas de disponibilidad. Elastic Load Balancing ofrece tres tipos de balanceadores de carga que cuentan con el nivel necesario de alta disponibilidad, escalabilidad automática y seguridad para que sus aplicaciones sean tolerantes a errores.

### Grupos de autoscaling
[Amazon EC2 Auto Scaling](https://docs.aws.amazon.com/autoscaling/ec2/userguide/what-is-amazon-ec2-auto-scaling.html) permite garantizar una cantidad correcta de instancias de Amazon EC2 disponibles para manejar la carga de una aplicación. Permite crear colecciones de instancias EC2, llamadas grupos de Auto Scaling en donde se especifican el número mínimo de instancias en cada grupo de Auto Scaling, y Amazon EC2 Auto Scaling garantiza que un grupo nunca tenga este número de instancias. Se puede especificar el número máximo de instancias en cada grupo de Auto Scaling, y Amazon EC2 Auto Scaling garantiza que un grupo nunca supere este tamaño. Se especifica capacidad y políticas de escalado de donde Amazon EC2 Auto Scaling inicia o finaliza instancias a medida que aumenta o disminuye la demanda de la aplicación.
